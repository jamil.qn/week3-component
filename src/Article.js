import React from "react";

const Article = (props) => {
  return (
    <div class="ui card">
      <div class="content">
        <div class="header">{props.articleInfo.header}</div>
        <div class="meta">{props.children}</div>
        <div class="description">{props.articleInfo.description}</div>
      </div>
    </div>
  );
};

export default Article;
