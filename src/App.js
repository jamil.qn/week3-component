import React from "react";
import Article from "./Article";
import PublisherDetail from "./PublisherDetail";
import Faker from "faker";

const getDate = () => {
  return new Date().toLocaleDateString();
};

function App() {
  return (
    <div className="ui container">
      <div className="ui cards">
        <Article
          articleInfo={{
            header: "lorem ipsum",
            description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
          }}
        >
          <PublisherDetail
            detail={{
              name: "John",
              avatar: Faker.image.avatar(),
              date: getDate(),
            }}
          />
        </Article>

        <Article
          articleInfo={{
            header: "lorem ipsum",
            description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
          }}
        >
          <PublisherDetail
            detail={{
              name: "jamil",
              avatar: Faker.image.avatar(),
              date: getDate(),
            }}
          />
        </Article>

        <Article
          articleInfo={{
            header: "lorem ipsum",
            description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
          }}
        >
          <PublisherDetail
            detail={{
              name: "jamil",
              avatar: Faker.image.avatar(),
              date: getDate(),
            }}
          />
        </Article>
      </div>
    </div>
  );
}

export default App;
