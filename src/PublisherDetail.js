import React from "react";
const PublisherDetail = (props) => {
  return (
    <div className="comment">
      <a href="/" className="avatar">
        <img alt="avatar" src={props.detail.avatar} />
      </a>
      <div className="content">
        <a href="/" className="author">
          Published By: {props.detail.name}
        </a>
        <div className="metadata">
          <span className="data">Date: {props.detail.date}</span>
        </div>
      </div>
    </div>
  );
};

export default PublisherDetail;
